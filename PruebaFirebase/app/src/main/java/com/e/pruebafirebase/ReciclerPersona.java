package com.e.pruebafirebase;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pruebafirebase.adapter.AdapterPersona;
import com.e.pruebafirebase.databinding.ActivityRecyclerpersonaBinding;
import com.e.pruebafirebase.Model.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReciclerPersona extends AppCompatActivity {

    private ActivityRecyclerpersonaBinding mainBinding;
    Button b1;

    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityRecyclerpersonaBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);
        mainBinding.reciclerPais.setHasFixedSize(true);
        db = FirebaseFirestore.getInstance();
        consultarListaPersona();


    }



    public void cargar(List<Persona> personas) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mainBinding.reciclerPais.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new AdapterPersona(personas);
        mainBinding.reciclerPais.setAdapter(mAdapter);
    }

    private void consultarListaPersona() {
        db.collection("Empleado")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Persona> personas = new ArrayList<>();
                            //personas = task.getResult().toObjects(Persona.class);
                            //usando json a diccionarios
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();

                                String nombre = (String) md.get("Nombre");
                                String correo = (String) md.get("Correo");

                                personas.add(new Persona(nombre,correo));
                            }
                            cargar(personas);
                        }
                    }
                });
    }







}