package com.e.pruebafirebase.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pruebafirebase.LoginActivity;
import com.e.pruebafirebase.MainActivity;
import com.e.pruebafirebase.R;


import java.util.List;
import com.e.pruebafirebase.Model.*;
import com.e.pruebafirebase.ReciclerComentarios;
import com.e.pruebafirebase.ReciclerPersona;

public class AdapterPersona extends RecyclerView.Adapter<AdapterPersona.MyViewHolder>  {

    public List<Persona>listapersona;

    public AdapterPersona(List<Persona> person){
        this.listapersona = person;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_personas,parent,false);
        return new MyViewHolder(v);


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position){

        Persona persona=listapersona.get(position);
        holder.nombre.setText(persona.getNombre());
        holder.correo.setText(persona.getCorreo());
        holder.listerner();



    }

    @Override
    public int getItemCount(){

        return listapersona.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        Context context;
        public TextView nombre,correo;
        public Button b1;


        public MyViewHolder(View view){
            super(view);
            context = view.getContext();
            nombre= view.findViewById(R.id.Nombre);
            correo=view.findViewById(R.id.Correo);
            b1 = view.findViewById(R.id.ingresar);


        }

        void listerner(){
            b1.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent  i1 = new Intent(context, ReciclerComentarios.class);
            i1.putExtra("Nombre",nombre.getText());
            i1.putExtra("Correo",correo.getText());
            context.startActivity(i1);
        }
    }


}
