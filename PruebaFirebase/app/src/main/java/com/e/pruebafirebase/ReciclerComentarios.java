package com.e.pruebafirebase;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReciclerComentarios extends AppCompatActivity {

    TextView t1, t2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        t1 = findViewById(R.id.setearNombre);
        t2 = findViewById(R.id.setearCorreo);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
             String value =extras.getString("Nombre");
            String value2 =extras.getString("Correo");
            t1.setText(value);
            t2.setText(value2);
        }
    }
}
