package com.e.pruebafirebase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.e.pruebafirebase.Model.Persona;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText e1, e2;
    Button b1,b2,b3;
    CollectionReference per;


    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        e1 = findViewById(R.id.usuario);
        e2 = findViewById(R.id.contrasena);
        db = FirebaseFirestore.getInstance();
        b1 = findViewById(R.id.ingresar);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cor = e1.getText().toString();
                String con = e2.getText().toString();
                consultar(cor,con);
            }
       });
     b2 = findViewById(R.id.registrarse);
     b2.setOnClickListener(new View.OnClickListener() {
         @Override
           public void onClick(View view) {
                Intent b2 = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(b2);
          }

      });
 }

  public void consultar(String usuario, String contrasena ){


      per = db.collection("Empleado");
          Query query = per.whereEqualTo("Correo", usuario).whereEqualTo("Password", contrasena);
          query.get()
                  .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                      @Override
                      public void onComplete(Task<QuerySnapshot> task) {
                          if (task.isSuccessful()) {
                              for (QueryDocumentSnapshot document : task.getResult()) {
                                  Map<String,Object> md = document.getData();
                                  Intent b3= new Intent(LoginActivity.this, EmpleadoActivity.class);
                                  startActivity(b3);

                              }

                          } else {
                              System.out.println("Error");
                          }
                      }


                  });

          per =db.collection("Empleador");
          Query query2 = per.whereEqualTo("Correo", usuario).whereEqualTo("Password", contrasena);
          query2.get()
                  .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                      @Override
                      public void onComplete(Task<QuerySnapshot> task) {
                          if (task.isSuccessful()) {
                              for (QueryDocumentSnapshot document : task.getResult()) {
                                  Intent b1 = new Intent(LoginActivity.this, ReciclerPersona.class);
                                  startActivity(b1);

                              }

                          } else {
                              System.out.println("Error");
                          }
                      }


                  });

      }
  }

