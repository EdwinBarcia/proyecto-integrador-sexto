package com.e.pruebafirebase.Model;

public class Persona {
    private String id;
    private String nombre;
    private String apellido;
    private String correo;
    private String password;

    public Persona(){

    }

    public Persona(String Nombre, String correo){
        this.nombre = Nombre;
        this.correo = correo;

    }
    public Persona(String id, String nombre, String apellido, String correo ,String password) {
        this.id = id;
        this.nombre= nombre;
        this.apellido= apellido;
        this.correo= correo;
        this.password = password;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
